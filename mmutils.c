#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <mastermind.h>


// REMEMBER:  ALL THE FUNCTIONS IN THIS FILE NEED TO HAVE APPROPRIATE
//            FUNCTION COMMENTS WRITTEN FOR THEM.
//            DO NOT SUBMIT THIS ASSIGNMENT WITHOUT THEM.



void say_goodbye_and_exit_program( const char *message )
{
        fprintf( stderr, "%s\n", message );
        exit( EXIT_FAILURE );
}


void generate_random_code( peg_array code )
{
        // STUB

        return;
}


char *peg_array_tostring( const peg_array c )
{
        static char buf[ CODE_STRING_MAXIMUM_SIZE ];

        char *peg1 = mastermind_peg_tostring( c[0] );
        char *peg2 = mastermind_peg_tostring( c[1] );
        char *peg3 = mastermind_peg_tostring( c[2] );
        char *peg4 = mastermind_peg_tostring( c[3] );

        snprintf( buf, CODE_STRING_MAXIMUM_SIZE, "|%-6s| |%-6s| |%-6s| |%-6s|", peg1, peg2, peg3, peg4 );

        return buf;
}


char *key_array_tostring( const key_array c )
{
        static char buf[ CODE_STRING_MAXIMUM_SIZE ];

        char *k1 = mastermind_key_tostring( c[0] );
        char *k2 = mastermind_key_tostring( c[1] );
        char *k3 = mastermind_key_tostring( c[2] );
        char *k4 = mastermind_key_tostring( c[3] );

        snprintf( buf, CODE_STRING_MAXIMUM_SIZE, "[%-5s] [%-5s] [%-5s] [%-5s]", k1, k2, k3, k4 );

        return buf;
}



char *mastermind_peg_tostring( const mastermind_peg p )
{
        char *rc = (char *)DEFAULT_RETURN_STRING;

        switch ( p ) {
                case Blue:   rc = "Blue";    break;
                case Yellow: rc = "Yellow";  break;
                case Green:  rc = "Green";   break;
                case Purple: rc = "Purple";  break;
                case Orange: rc = "Orange";  break;
                case Black:  rc = "Black";   break;
                default: ; // none
        }

        return rc;
}

