# mastermind_starter

This lab assignment is about using existing code and updating it to include all the requirements for the lab.
It will be the first time we use multiple source code files, a lab9.c file and a mmutils.c file.  You will 
note the mmutils.c file does NOT have a main() function.  That is because there can be only one main() 
function in any given program, and our main function is in lab9.c.

To compile this program as it is, just to confirm all is well, use the compiler invocation:

    cc -I. mmutils.c lab9.c

You will get errors about missing functions, which really means it is working.  Here's the error I get:

    starter> cc -I. mmutils.c lab9.c
    /tmp/ccYWfmwV.o: In function `key_array_tostring':
    mmutils.c:(.text+0xec): undefined reference to `mastermind_key_tostring'
    mmutils.c:(.text+0x101): undefined reference to `mastermind_key_tostring'
    mmutils.c:(.text+0x116): undefined reference to `mastermind_key_tostring'
    mmutils.c:(.text+0x12b): undefined reference to `mastermind_key_tostring'
    /tmp/ccCPJR72.o: In function `main':
    lab9.c:(.text+0x1d): undefined reference to `program_identification'
    lab9.c:(.text+0x8a): undefined reference to `mastermind_response'
    collect2: error: ld returned 1 exit status
    starter>

To get it to compile successfully you will need to create those missing functions as described in the assignment and then rerun the compile command above.

Put those functions into the mmutils.c file along with all the other functions, and make sure to comment all of them properly. 

Also be sure to put any necessary function prototypes into the mastermind.h file.  Your *.c files should not
have any prototypes in them.  They also shouldn't have any preprocessor directives other than #includes, and
should not have any global constants. 

When the program runs, it should look something like the following:

    mastermind_starter> ./a.out
    This program was written by Sally Student and was compiled on Apr  6 2022 at 16:30:28.
    ASSIGNMENT NAME: Lab 9: Mastermind
    THIS PROGRAM:
    Plays the game of Mastermind!


    Given the following colors:
        (b)lue, (y)ellow, (g)reen, (p)urple, (o)range, b(l)ack
    enter your 4 color guesses using the letter in parentheses or 0 to quit: ygyl
    Your guess is |Yellow| |Green | |Yellow| |Black |.

        Peg 3 is right color in right position.
        Peg 0 is right color but in the wrong position.
        Peg 1 is right color but in the wrong position.

    The mastermind responds: [Red  ] [White] [White] [Empty].

    Given the following colors:
        (b)lue, (y)ellow, (g)reen, (p)urple, (o)range, b(l)ack
    enter your 4 color guesses using the letter in parentheses or 0 to quit: lygl
    Your guess is |Black | |Yellow| |Green | |Black |.

        Peg 0 is right color in right position.
        Peg 1 is right color in right position.
        Peg 2 is right color in right position.
        Peg 3 is right color in right position.

    The mastermind responds: [Red  ] [Red  ] [Red  ] [Red  ].

    Congratulations, you beat the mastermind code in only 2 tries!
    mastermind_starter>
